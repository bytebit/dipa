/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author abraham
 */
public final class Db {
    
     private final static DataSource ds = initDs();
    
    static DataSource initDs() {
        try {
            Context context = new InitialContext();
            Context envCont = (Context)context.lookup("java:comp/env");
            return (DataSource)envCont.lookup("jdbc/news");
        } catch (NamingException ex) {
            //something went realy realy wrong, like db name changed!
            Logger.getLogger(Db.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    } 
    
    
    
    public static Connection getConnection() throws NamingException, SQLException {
           return ds.getConnection();
    }

    
     //static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
   //static final String DB_URL = "jdbc:mysql://localhost/javatutorial";
    
   
   
   //for junit test
    /*
   public static Connection getConnection() throws NamingException, SQLException  {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {          
            Logger.getLogger(Db.class.getName()).log(Level.SEVERE, null, ex);
            throw new NamingException();
        }
        try {
            return DriverManager.getConnection(DB_URL,"javatutorial","1zeronerone");
        } catch (SQLException ex) {
            Logger.getLogger(Db.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
   }
*/
   
    
    
    
    
}
