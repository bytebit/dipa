/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author abraham
 */
public class Utils {
    
    //pass http get param with  key=API_KEY 
    public static final String GOOGLE_MAPS_API_KEY = "AIzaSyAjJMBS5IiF_vrYB1gNRpcdQ4BymcR0CSA";
    //pass with  key=API_KEY 
    public static final String GOOGLE_PLACES_API_KEY = "AIzaSyCvLA48w_-h-y5eBvDjuQwLj22k6ZsYAgo";
    //pass with key=API_KEY
    public static final String GOOGLE_GEOLOCATING_API_KEY = "AIzaSyC5x-ghRtZ1M8-9rcUo1ZwAUsZqc9dof18";
    
    //@see api http rest google tutorials
    //https://developers.google.com/maps/documentation/geocoding/start
    //https://developers.google.com/places/web-service/
    //https://developers.google.com/maps/documentation/geolocation/intro
    
    
    public static String getCityNameFromJson(String json) {
        String cityName = "";
        JSONObject ob = new JSONObject(json);
        JSONArray arr = ob.getJSONArray("results");
        
        JSONObject ob2 = arr.getJSONObject(0);
        JSONArray ob3 = ob2.getJSONArray("address_components");
        for(int i =0; i < ob3.length(); i++) {
            JSONObject o = ob3.getJSONObject(i);
            JSONArray arrc = o.getJSONArray("types");
            if(!arrc.isNull(0) && arrc.getString(0).equals("administrative_area_level_3")) {   
                if(!o.isNull("long_name")) {
                    String city = o.getString("long_name");
                    cityName = city;
                    break;
                }
                //in case administrative_area_level_3 is absent get administrative_area_level_2
            } else if(!arrc.isNull(0) && arrc.getString(0).equals("administrative_area_level_2")){
                 if(!o.isNull("long_name")) {
                    String city = o.getString("long_name");
                    cityName = city;
                }
            }
        }
        
        return cityName;
    }
    
}
