/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author zeron
 */
public class Test {
    
    //change to main to test
    public static void mains(String[] a) {
        
        
        JSONObject job = new JSONObject(json);
        
        JSONArray arr = job.getJSONArray("results");
        
        for(int i =0; i < arr.length(); i++) {
            JSONObject temp  = arr.getJSONObject(i);
            String name = temp.getString("name");
            String place_id = temp.getString("place_id");
            JSONArray photoarr = temp.getJSONArray("photos");
            System.out.println(name);
            System.out.println(place_id);
            for(int x=0; x < photoarr.length(); x++) {
                JSONObject tempob = photoarr.getJSONObject(x);
                String photo_ref = tempob.getString("photo_reference");
                System.out.println(photo_ref);
                break;
            }
                
        }
        
    }
    
    static String json = "{\n" +
"   \"html_attributions\" : [],\n" +
"   \"results\" : [\n" +
"      {\n" +
"         \"geometry\" : {\n" +
"            \"location\" : {\n" +
"               \"lat\" : 51.5011719,\n" +
"               \"lng\" : -0.1256035\n" +
"            },\n" +
"            \"viewport\" : {\n" +
"               \"northeast\" : {\n" +
"                  \"lat\" : 51.50125084999999,\n" +
"                  \"lng\" : -0.12560185\n" +
"               },\n" +
"               \"southwest\" : {\n" +
"                  \"lat\" : 51.50093505,\n" +
"                  \"lng\" : -0.12560405\n" +
"               }\n" +
"            }\n" +
"         },\n" +
"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/bar-71.png\",\n" +
"         \"id\" : \"ca52682957e57e6a0093627ae1660976ca834736\",\n" +
"         \"name\" : \"St Stephen's Tavern\",\n" +
"         \"opening_hours\" : {\n" +
"            \"open_now\" : true,\n" +
"            \"weekday_text\" : []\n" +
"         },\n" +
"         \"photos\" : [\n" +
"            {\n" +
"               \"height\" : 720,\n" +
"               \"html_attributions\" : [\n" +
"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/116601561213072735821/photos\\\"\\u003ePavel Simek\\u003c/a\\u003e\"\n" +
"               ],\n" +
"               \"photo_reference\" : \"CoQBdwAAABmGTAPhQJ33IPW5uR2JyrCwC-ik2PNlq_Yz9IWhTDx9SABVupEyfcx45Ii7gawQPCM_pI83JY17F5pagVUIL9TYYZtA2eReO6udCdCxrEr5wrXbvrtbIS-WBwdeEweJqVh1ot08g_POTC6-9OyfEKCv4za48oWiBArkCq4-f9PjEhBcX9OtV_kVoR8SQDE46PRNGhTOtOTD2fBbktsjnXzYnqQm4iOIWg\",\n" +
"               \"width\" : 1280\n" +
"            }\n" +
"         ],\n" +
"         \"place_id\" : \"ChIJZSfzRMQEdkgRKp5LnanZ2KM\",\n" +
"         \"rating\" : 3.9,\n" +
"         \"reference\" : \"CmRSAAAAXNcj_g_W_vYhMaw5HuzDTNQKKMpPIcmrw_mR4vzHRP04Rp-BzM6OEUC6Z0XVyzipdKm315yd4G8xU_Je_aT7O7CxWbW6Ppy8ggKR2hPN8up62Zpq-IGa1tyb5ISKF8PaEhB3AepllOS-fvQutIAvXwT5GhSHJnEchPGCtus1DJFHJ524x_cKAA\",\n" +
"         \"scope\" : \"GOOGLE\",\n" +
"         \"types\" : [ \"bar\", \"restaurant\", \"food\", \"point_of_interest\", \"establishment\" ],\n" +
"         \"vicinity\" : \"10 Bridge Street, London\"\n" +
"      },\n" +
"      {\n" +
"         \"geometry\" : {\n" +
"            \"location\" : {\n" +
"               \"lat\" : 51.5020755,\n" +
"               \"lng\" : -0.1257049\n" +
"            },\n" +
"            \"viewport\" : {\n" +
"               \"northeast\" : {\n" +
"                  \"lat\" : 51.50217540000002,\n" +
"                  \"lng\" : -0.12553115\n" +
"               },\n" +
"               \"southwest\" : {\n" +
"                  \"lat\" : 51.50204219999999,\n" +
"                  \"lng\" : -0.12622615\n" +
"               }\n" +
"            }\n" +
"         },\n" +
"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/bar-71.png\",\n" +
"         \"id\" : \"544ddf30de51b677860f3cd73dafd37c4abd950f\",\n" +
"         \"name\" : \"The Red Lion\",\n" +
"         \"opening_hours\" : {\n" +
"            \"open_now\" : true,\n" +
"            \"weekday_text\" : []\n" +
"         },\n" +
"         \"photos\" : [\n" +
"            {\n" +
"               \"height\" : 501,\n" +
"               \"html_attributions\" : [\n" +
"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/112913170889421783739/photos\\\"\\u003eThe Red Lion\\u003c/a\\u003e\"\n" +
"               ],\n" +
"               \"photo_reference\" : \"CoQBdwAAAJPdO2vFCccaltuN-toXX5LGMtFqaAFDzK1xe_PTxb6XDVkVo6EJOWpoq2dPzoPl_G1GwXVKvvyQRbsyYA7XKm7sIrx1o6cwuwEa8_5qS9VQH4DTQKiAP7WR6F5Rw8rOb0VjpnVWK43bDdtnsYYkdJBTU9gg0iTpSHKQ4MLsyi6oEhDdW6gohEORnR9k63gDoFenGhQFsonohUQDbh-NXDaRMAeJaw99xg\",\n" +
"               \"width\" : 892\n" +
"            }\n" +
"         ],\n" +
"         \"place_id\" : \"ChIJMQcHrMUEdkgRCvMbPS0cuss\",\n" +
"         \"rating\" : 4,\n" +
"         \"reference\" : \"CmRSAAAAJmFQDJaGTtRdgU9Il98QfTal3lkXF_i4GflAQLrHHiq0iCBZmXO5oD_NqJyypKiGqwUq8vY9KmOeIVp1qCwojy_Qa6I5LH84Eowpf0lOQqLqDyUzi-m6Yaqfyp8UaDv3EhA1Ngd8wuIE82NdZdM005ZyGhRQ_KvaHsyHS6ZknzeQyt_R5rh-TQ\",\n" +
"         \"scope\" : \"GOOGLE\",\n" +
"         \"types\" : [ \"bar\", \"restaurant\", \"food\", \"point_of_interest\", \"establishment\" ],\n" +
"         \"vicinity\" : \"48 Parliament Street, London\"\n" +
"      },\n" +
"      {\n" +
"         \"geometry\" : {\n" +
"            \"location\" : {\n" +
"               \"lat\" : 51.5009563,\n" +
"               \"lng\" : -0.1282893\n" +
"            },\n" +
"            \"viewport\" : {\n" +
"               \"northeast\" : {\n" +
"                  \"lat\" : 51.50127835000001,\n" +
"                  \"lng\" : -0.12827895\n" +
"               },\n" +
"               \"southwest\" : {\n" +
"                  \"lat\" : 51.50084895,\n" +
"                  \"lng\" : -0.12830315\n" +
"               }\n" +
"            }\n" +
"         },\n" +
"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png\",\n" +
"         \"id\" : \"b3c40afa612bba8abd78f2d4ab37dbeac469ed03\",\n" +
"         \"name\" : \"Roux at Parliament Square\",\n" +
"         \"opening_hours\" : {\n" +
"            \"open_now\" : true,\n" +
"            \"weekday_text\" : []\n" +
"         },\n" +
"         \"photos\" : [\n" +
"            {\n" +
"               \"height\" : 2048,\n" +
"               \"html_attributions\" : [\n" +
"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/104111246635874032234/photos\\\"\\u003eZAGAT\\u003c/a\\u003e\"\n" +
"               ],\n" +
"               \"photo_reference\" : \"CoQBcwAAANcQpFI7Q0zc8oMKQI3PUBDsmvwGYRpyZJocDsU_dzwEubx1wAD0AUGRSx0Rpy_tlVr4McQJrpxy64iEbgzz8PDBiZR7E7y9aihMtSFwyweXHpf7iI79ox0_PkBDmeV9-hzF2PZCub2qXiLF_RVborxmuB3TFAWcJiXZvkYZIA5tEhBuoGM27H3kKilmbnMrVuAYGhQOOi_7I7ousf5H1PYhzYxhGUxleA\",\n" +
"               \"width\" : 1365\n" +
"            }\n" +
"         ],\n" +
"         \"place_id\" : \"ChIJUT0R0MQEdkgRcrp3AShwATI\",\n" +
"         \"price_level\" : 4,\n" +
"         \"rating\" : 4.7,\n" +
"         \"reference\" : \"CmRRAAAA_Hx-h1jiIMRURTXyp0G1ncqazBai1JcgI6l8Y3DZyu8PGLGuEGUOhiFAHY3ejuawstpkZgDeoSUfl95YQn-OUlDOqa0Y34lI7F2NYe3Zh0zEmp7tQcFMLR4s7pDY7Oh-EhBmh25BHaGjl6WeILIaywtbGhTW_il19TR3x1RBHdiCCpuysSWt6A\",\n" +
"         \"scope\" : \"GOOGLE\",\n" +
"         \"types\" : [ \"bar\", \"restaurant\", \"food\", \"point_of_interest\", \"establishment\" ],\n" +
"         \"vicinity\" : \"11 Great George Street, London\"\n" +
"      },\n" +
"      {\n" +
"         \"geometry\" : {\n" +
"            \"location\" : {\n" +
"               \"lat\" : 51.506134,\n" +
"               \"lng\" : -0.126791\n" +
"            },\n" +
"            \"viewport\" : {\n" +
"               \"northeast\" : {\n" +
"                  \"lat\" : 51.50615675,\n" +
"                  \"lng\" : -0.12669505\n" +
"               },\n" +
"               \"southwest\" : {\n" +
"                  \"lat\" : 51.50606575,\n" +
"                  \"lng\" : -0.12707885\n" +
"               }\n" +
"            }\n" +
"         },\n" +
"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/bar-71.png\",\n" +
"         \"id\" : \"48dd3942c644c7e0c1ef36457c3c605a331b4294\",\n" +
"         \"name\" : \"The Clarence, Whitehall\",\n" +
"         \"opening_hours\" : {\n" +
"            \"open_now\" : true,\n" +
"            \"weekday_text\" : []\n" +
"         },\n" +
"         \"photos\" : [\n" +
"            {\n" +
"               \"height\" : 2035,\n" +
"               \"html_attributions\" : [\n" +
"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/109809023964299678791/photos\\\"\\u003eThe Clarence, Whitehall\\u003c/a\\u003e\"\n" +
"               ],\n" +
"               \"photo_reference\" : \"CoQBdwAAAElDIJoe1OUhFf8Fe9aUsKR3eUDeFpUwbAk_Vd2AvjUH5Pl48JCAKa6jeuMOcEfDQQe61PW5yMjpKc9Qsz4bkUHNRt2F3aYluiy6_SvXQdV6BHTRbFHJMopn7hUxxF-1tsqph_YSAm6P0pctjePv9Zv17M8jRhN-9KLveeAKmpWdEhChj9ptRVUofx89uJ6tkuifGhTVDWdjHl_neBjVAeHy_G1QjA2J3g\",\n" +
"               \"width\" : 2035\n" +
"            }\n" +
"         ],\n" +
"         \"place_id\" : \"ChIJiaV7o88EdkgRvDTL3HQBHww\",\n" +
"         \"rating\" : 3.7,\n" +
"         \"reference\" : \"CmRRAAAAvy8ncs2vO0YhHW_NDLh83QRH8c7XX43NxggAVSn7buq6SbzU8af1jF_SFzpq0P6iLe9a9C1YBP92qOTT9OeOQVUQW07CGILaqmnWH7LKb9SY_XmC55-LlDNW57rXwXXoEhBrwzWJEiW1ivdccUX4Lm2AGhSMxCr-iupM9W6L8mq7OiRK4_bnyg\",\n" +
"         \"scope\" : \"GOOGLE\",\n" +
"         \"types\" : [ \"bar\", \"restaurant\", \"food\", \"point_of_interest\", \"establishment\" ],\n" +
"         \"vicinity\" : \"53 Whitehall, London\"\n" +
"      },\n" +
"      {\n" +
"         \"geometry\" : {\n" +
"            \"location\" : {\n" +
"               \"lat\" : 51.50592539999999,\n" +
"               \"lng\" : -0.1242052\n" +
"            },\n" +
"            \"viewport\" : {\n" +
"               \"northeast\" : {\n" +
"                  \"lat\" : 51.50625510000001,\n" +
"                  \"lng\" : -0.12414475\n" +
"               },\n" +
"               \"southwest\" : {\n" +
"                  \"lat\" : 51.50581549999998,\n" +
"                  \"lng\" : -0.12438655\n" +
"               }\n" +
"            }\n" +
"         },\n" +
"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png\",\n" +
"         \"id\" : \"384a69336079af5b4d18b82b3241d54af221bec9\",\n" +
"         \"name\" : \"The Lounge at The Royal Horseguards\",\n" +
"         \"opening_hours\" : {\n" +
"            \"open_now\" : true,\n" +
"            \"weekday_text\" : []\n" +
"         },\n" +
"         \"photos\" : [\n" +
"            {\n" +
"               \"height\" : 320,\n" +
"               \"html_attributions\" : [\n" +
"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/110045806135192515027/photos\\\"\\u003eThe Lounge at The Royal Horseguards\\u003c/a\\u003e\"\n" +
"               ],\n" +
"               \"photo_reference\" : \"CoQBcwAAACp0PXR0MPuzF0r7qltb1WCquJzwO7HGhbwr2PT2WSE9JJZsjT-l6hbJsPSXEG0bJ0cYYNRG24_wZDyCd3ZqhG1j83Bg2gp7Ng4JfJ9zAKgqWiSpc01bSCdztUkjaLrVrPJvuEjRqJQuaQEUsw4ln382O0E6j-u33jsaaLCiBXUjEhAqBy5GvmaL7tN3-R5LHXtEGhSzQcgrMfq9lr8sqHodfh4SKBqleA\",\n" +
"               \"width\" : 430\n" +
"            }\n" +
"         ],\n" +
"         \"place_id\" : \"ChIJc9zIO88EdkgR6kSDqM40Kzk\",\n" +
"         \"reference\" : \"CmRRAAAAYmTgR5JiIgPNuQYuhVh08i8BfI_i8rX3CWU1jsBJUJQ9EeB9B3eTZabae70b1GuJk4KmOlvpQq0W0xsyBzM-6zIixW5i9Ez-l_wC0AGo_puY7FmKRGHISpx19yQ6rf3NEhBLTQV8AnA3uDHuSDH7Ti2_GhS4lAI2IXmWcTCaFzvxG8NEE9grBA\",\n" +
"         \"scope\" : \"GOOGLE\",\n" +
"         \"types\" : [\n" +
"            \"cafe\",\n" +
"            \"bar\",\n" +
"            \"lodging\",\n" +
"            \"food\",\n" +
"            \"point_of_interest\",\n" +
"            \"establishment\"\n" +
"         ],\n" +
"         \"vicinity\" : \"The Royal Horseguards, 2 Whitehall Court, Whitehall, London\"\n" +
"      },\n" +
"      {\n" +
"         \"geometry\" : {\n" +
"            \"location\" : {\n" +
"               \"lat\" : 51.50098860000001,\n" +
"               \"lng\" : -0.1284337\n" +
"            }\n" +
"         },\n" +
"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png\",\n" +
"         \"id\" : \"c891d90559b6151010b435abcb1c5152f615a811\",\n" +
"         \"name\" : \"Mobile Bar Hire\",\n" +
"         \"place_id\" : \"ChIJ97aDz8QEdkgRIUgjqLPkkMA\",\n" +
"         \"reference\" : \"CmRSAAAA7cbG2gg4VZgkS0jpNEp3xkcV_-arYujmNf51PtV3Qv6obAuUHcS24f62wwJCkgv7FQQ2d9fCX4Y0ev4L8OAtqqLC2i9FdW8vVf5PR9tk-7AE45gK8TXjgZ1dJ35t9taoEhAfDTG5BAcWrTk2uEZQ_paoGhRLMIvKdw2OKSMlJx65z0IQTUFpuA\",\n" +
"         \"scope\" : \"GOOGLE\",\n" +
"         \"types\" : [ \"bar\", \"food\", \"point_of_interest\", \"establishment\" ],\n" +
"         \"vicinity\" : \"London\"\n" +
"      }\n" +
"   ],\n" +
"   \"status\" : \"OK\"\n" +
"}";

}
