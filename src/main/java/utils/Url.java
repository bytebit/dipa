/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author abraham
 */
public class Url {
    
    //prefered json
    public static final String GOOGLE_MAPS = "https://maps.googleapis.com/maps/api/geocode/json?";
    public static final String GOOGLE_PLACES = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
    public static final String GOOGLE_PLACE = "https://maps.googleapis.com/maps/api/place/details/json?";
    public static final String GOOGLE_PHOTO = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&";
    
    
    
    public static final String GOOGLE_MAPS_XML = "https://maps.googleapis.com/maps/api/geocode/xml?";
    public static final String GOOGLE_PLACES_XML = "https://maps.googleapis.com/maps/api/place/nearbysearch/xml?";
    
}
