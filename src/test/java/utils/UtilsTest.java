/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author abraham
 */
public class UtilsTest {
    
    public UtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testGetPlace_id() {
        JSONObject ob = new JSONObject(mockJson);       
        JSONArray arr = ob.getJSONArray("results");       
        List<String> placeid = new ArrayList<String>();
        
        for(int i =0; i < arr.length(); i++) {
            
            String id = arr.getJSONObject(i).getString("place_id");
            JSONArray arr2 = arr.getJSONObject(i).getJSONArray("photos");
            for(int x=0; x < arr2.length(); x++) {          
                String photo = arr2.getJSONObject(x).getString("photo_reference");                
            }
            
        }
    }
    
    
    @Test
    public void testGetPlacePhoto_references() {
         JSONObject ob = new JSONObject(mockJson2);
        JSONObject obj = ob.getJSONObject("result");
        JSONArray arr = obj.getJSONArray("photos");
        
        List<String> placeid = new ArrayList<String>();
        for(int i =0; i < arr.length(); i++) {
            String photo = arr.getJSONObject(i).getString("photo_reference");
        }
    }

    
    /**
     * Test of getCityNameFromJson method, of class Utils.
     */
    @Test
    public void testGetCityNameFromJson() {
        String cityName = "";
        JSONObject ob = new JSONObject(mockJsonLocation);
        JSONArray arr = ob.getJSONArray("results");
        
        JSONObject ob2 = arr.getJSONObject(0);
        JSONArray ob3 = ob2.getJSONArray("address_components");
        for(int i =0; i < ob3.length(); i++) {
            JSONObject o = ob3.getJSONObject(i);
            JSONArray arrc = o.getJSONArray("types");
            if(!arrc.isNull(0) && arrc.getString(0).equals("administrative_area_level_3")) {   
                if(!o.isNull("long_name")) {
                    String city = o.getString("long_name");
                    cityName = city;
                    break;
                }
                //in case administrative_area_level_3 is absent get administrative_area_level_2
            } else if(!arrc.isNull(0) && arrc.getString(0).equals("administrative_area_level_2")){
                 if(!o.isNull("long_name")) {
                    String city = o.getString("long_name");
                    cityName = city;
                }
            }
        }
        
       
    }
    
    
    
    private static String mockJson2 = "{\n" +
"   \"html_attributions\" : [],\n" +
"   \"result\" : {\n" +
"      \"address_components\" : [\n" +
"         {\n" +
"            \"long_name\" : \"52\",\n" +
"            \"short_name\" : \"52\",\n" +
"            \"types\" : [ \"street_number\" ]\n" +
"         },\n" +
"         {\n" +
"            \"long_name\" : \"Leoforos Thessalonikis - Michanionas\",\n" +
"            \"short_name\" : \"Leof. Thessalonikis - Michanionas\",\n" +
"            \"types\" : [ \"route\" ]\n" +
"         },\n" +
"         {\n" +
"            \"long_name\" : \"Perea\",\n" +
"            \"short_name\" : \"Perea\",\n" +
"            \"types\" : [ \"locality\", \"political\" ]\n" +
"         },\n" +
"         {\n" +
"            \"long_name\" : \"Thermaikos\",\n" +
"            \"short_name\" : \"Thermaikos\",\n" +
"            \"types\" : [ \"administrative_area_level_5\", \"political\" ]\n" +
"         },\n" +
"         {\n" +
"            \"long_name\" : \"Thermaikos\",\n" +
"            \"short_name\" : \"Thermaikos\",\n" +
"            \"types\" : [ \"administrative_area_level_4\", \"political\" ]\n" +
"         },\n" +
"         {\n" +
"            \"long_name\" : \"Thessaloniki\",\n" +
"            \"short_name\" : \"Thessaloniki\",\n" +
"            \"types\" : [ \"administrative_area_level_3\", \"political\" ]\n" +
"         },\n" +
"         {\n" +
"            \"long_name\" : \"Greece\",\n" +
"            \"short_name\" : \"GR\",\n" +
"            \"types\" : [ \"country\", \"political\" ]\n" +
"         },\n" +
"         {\n" +
"            \"long_name\" : \"570 19\",\n" +
"            \"short_name\" : \"570 19\",\n" +
"            \"types\" : [ \"postal_code\" ]\n" +
"         }\n" +
"      ],\n" +
"      \"adr_address\" : \"\\u003cspan class=\\\"street-address\\\"\\u003eLeof. Thessalonikis - Michanionas 52\\u003c/span\\u003e, \\u003cspan class=\\\"locality\\\"\\u003ePerea\\u003c/span\\u003e \\u003cspan class=\\\"postal-code\\\"\\u003e570 19\\u003c/span\\u003e, \\u003cspan class=\\\"country-name\\\"\\u003eGreece\\u003c/span\\u003e\",\n" +
"      \"formatted_address\" : \"Leof. Thessalonikis - Michanionas 52, Perea 570 19, Greece\",\n" +
"      \"formatted_phone_number\" : \"2392 022010\",\n" +
"      \"geometry\" : {\n" +
"         \"location\" : {\n" +
"            \"lat\" : 40.50157799999999,\n" +
"            \"lng\" : 22.9293817\n" +
"         },\n" +
"         \"viewport\" : {\n" +
"            \"northeast\" : {\n" +
"               \"lat\" : 40.50165604999999,\n" +
"               \"lng\" : 22.9293955\n" +
"            },\n" +
"            \"southwest\" : {\n" +
"               \"lat\" : 40.50153345,\n" +
"               \"lng\" : 22.92935749999999\n" +
"            }\n" +
"         }\n" +
"      },\n" +
"      \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png\",\n" +
"      \"id\" : \"a6a33062247e80d24e6ff20dc469d9c32b40af56\",\n" +
"      \"international_phone_number\" : \"+30 2392 022010\",\n" +
"      \"name\" : \"Lyco Lounge\",\n" +
"      \"photos\" : [\n" +
"         {\n" +
"            \"height\" : 2560,\n" +
"            \"html_attributions\" : [\n" +
"               \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/105355835927640564845/photos\\\"\\u003eΚΑΛΠΑΚΙΔΟΥ ΦΩΤΕΙΝΗ\\u003c/a\\u003e\"\n" +
"            ],\n" +
"            \"photo_reference\" : \"CoQBdwAAAKnGMnqKV5BEy4dQEPRJOpBdeVfmGteSyvibbXWVh2Ha7MynTZ8xJHVmVCl5gBIQ-kOKFVszyOzaNLRQyO6Ck_Iulqyz1La3_KgSg86A455h2hRuh50NrNr3WNulQpEDKOsepQOkcGnYTy7A9R3ESn5Uv0xO-5VsJ9dsEcR1RImLEhDV4mY1292XjG8dk65vrV9AGhQNLQAiflcxrEzhU9QUjXsUnL0BYw\",\n" +
"            \"width\" : 1920\n" +
"         },\n" +
"         {\n" +
"            \"height\" : 2322,\n" +
"            \"html_attributions\" : [\n" +
"               \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/107997781648693474908/photos\\\"\\u003eΠαυλος Βασενκο\\u003c/a\\u003e\"\n" +
"            ],\n" +
"            \"photo_reference\" : \"CoQBdwAAAJv-s5poRAHyVwflwRbWRss_TUBy0dUVetMfXkmO3texbe2YJcWO6U3lnXTrJVxeeaO3B-N0oaylopyRYIC9NCIh9jL0p4Zvvvoo459BHIdMLRPHmXmi4n9zq4USvkHSI2NH1Pa1H_8P_Ploao3uTJDLu8Rvuntdp6NgBjrK5-oQEhDtXeYmBjoxMW2QBYbP6NwmGhT2f9t_gV7AVze2ALNnOBeFs7QIAQ\",\n" +
"            \"width\" : 4128\n" +
"         }\n" +
"      ],\n" +
"      \"place_id\" : \"ChIJdTCudMAVqBQR4x6ZeZhMnFg\",\n" +
"      \"reference\" : \"CmRRAAAA6t29C2huZMT8G0-7S2XEn4qEVUdwi7GpDOv3bVG8oJx84X2RKkLLLxHBmYf1_teY2jSJU3hsZi9X-DYSdjGqgRObH_JvccTaBRTAxX8ZGJ-xK-sul-5HdsWO-IftdwUpEhCdd83OGAy7xDnlCLrTk2zaGhS10zj5oJI8qm_X_AtARUKgTLGjMw\",\n" +
"      \"scope\" : \"GOOGLE\",\n" +
"      \"types\" : [\n" +
"         \"restaurant\",\n" +
"         \"cafe\",\n" +
"         \"bar\",\n" +
"         \"food\",\n" +
"         \"point_of_interest\",\n" +
"         \"establishment\"\n" +
"      ],\n" +
"      \"url\" : \"https://maps.google.com/?cid=6385062589463338723\",\n" +
"      \"utc_offset\" : 180,\n" +
"      \"vicinity\" : \"Leoforos Thessalonikis - Michanionas 52, Perea\",\n" +
"      \"website\" : \"http://www.lyco-lounge.com/\"\n" +
"   },\n" +
"   \"status\" : \"OK\"\n" +
"}";
    
    
    private static String mockJson = "{\n" +
"   \"html_attributions\" : [],\n" +
"   \"results\" : [\n" +
"      {\n" +
"         \"geometry\" : {\n" +
"            \"location\" : {\n" +
"               \"lat\" : 40.5100193,\n" +
"               \"lng\" : 22.9280015\n" +
"            },\n" +
"            \"viewport\" : {\n" +
"               \"northeast\" : {\n" +
"                  \"lat\" : 40.51012129999999,\n" +
"                  \"lng\" : 22.9280317\n" +
"               },\n" +
"               \"southwest\" : {\n" +
"                  \"lat\" : 40.5099853,\n" +
"                  \"lng\" : 22.9279109\n" +
"               }\n" +
"            }\n" +
"         },\n" +
"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/bar-71.png\",\n" +
"         \"id\" : \"0d4494040fb8355ca860aa5db63cd126eb572da9\",\n" +
"         \"name\" : \"Fame Cafe Bar\",\n" +
"         \"opening_hours\" : {\n" +
"            \"open_now\" : true,\n" +
"            \"weekday_text\" : []\n" +
"         },\n" +
"         \"photos\" : [\n" +
"            {\n" +
"               \"height\" : 1920,\n" +
"               \"html_attributions\" : [\n" +
"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/104605312189584939935/photos\\\"\\u003eChristina Andromeda\\u003c/a\\u003e\"\n" +
"               ],\n" +
"               \"photo_reference\" : \"CoQBdwAAAERA7Vbcys-gfPyVku01jtFYbk-F0cHNLpP1DkGbTQMcVlXB487PdOyS0P7Hpg4brHa68jr74ulvDXTIVyRf-2MMR308JO7FSOuVaX35T71hy8ZU1OB5sI9MLNTKtvrX2ix6-0F9XmCThDVFF5HbXQfDiveGyRXV18Ro8iuEhX7NEhAX56X4A-pbp8hEMp_RxzKUGhTIkb4kYfXB5i7NcjLWkbaJpb860Q\",\n" +
"               \"width\" : 2560\n" +
"            }\n" +
"         ],\n" +
"         \"place_id\" : \"ChIJv0Lu3ewVqBQRmigpmx_u8Oc\",\n" +
"         \"rating\" : 4.4,\n" +
"         \"reference\" : \"CmRSAAAAJyS3NAp49ov8k3GQdXhD8lw34QT4POiI3wbRzT5F1tPmogmMSmLg2W7z3Bq5H8jnvidJzVOn1oY01QuoMHHdXWeUCFxSUb8wQiZBhFcj38q7x-koOo2gkioPxDw77UINEhBbX4CAEuwz84vaw_9kHgFJGhSriNCNuuk9Vqvbk-b-pslmJC7Orw\",\n" +
"         \"scope\" : \"GOOGLE\",\n" +
"         \"types\" : [ \"bar\", \"point_of_interest\", \"establishment\" ],\n" +
"         \"vicinity\" : \"47, Pavlou Kountouriotou, Perea\"\n" +
"      },\n" +
"      {\n" +
"         \"geometry\" : {\n" +
"            \"location\" : {\n" +
"               \"lat\" : 40.509153,\n" +
"               \"lng\" : 22.924816\n" +
"            },\n" +
"            \"viewport\" : {\n" +
"               \"northeast\" : {\n" +
"                  \"lat\" : 40.50919335,\n" +
"                  \"lng\" : 22.92485339999999\n" +
"               },\n" +
"               \"southwest\" : {\n" +
"                  \"lat\" : 40.50907514999999,\n" +
"                  \"lng\" : 22.9247966\n" +
"               }\n" +
"            }\n" +
"         },\n" +
"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/bar-71.png\",\n" +
"         \"id\" : \"c2d2f4f387c5735edb329e30910609b02f4aeed3\",\n" +
"         \"name\" : \"Paso All Day Cafe Bar\",\n" +
"         \"opening_hours\" : {\n" +
"            \"open_now\" : true,\n" +
"            \"weekday_text\" : []\n" +
"         },\n" +
"         \"photos\" : [\n" +
"            {\n" +
"               \"height\" : 681,\n" +
"               \"html_attributions\" : [\n" +
"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/113826154197957781182/photos\\\"\\u003ePaso All Day Cafe Bar\\u003c/a\\u003e\"\n" +
"               ],\n" +
"               \"photo_reference\" : \"CoQBcwAAAD9WXtR0NMKVkXjp8muRQ1KfK-bWf7Zt6k4-bbBmL0ajPkz0dsFvCezcntpYVZIpij5scpWmCk5Q2gjJAEkb7kO1pXvpwFUYFIuZMXGD7r_ZKJMULHqGQoKrlXvGzKhhrj8DtsflRNVfSsxAHm_zSngOoEHPitDmPYC-buFy6hvcEhCnyp5SSbNCgid7N7HLboknGhSNilhp4hMriJ3zGkHG332LnhRx5Q\",\n" +
"               \"width\" : 680\n" +
"            }\n" +
"         ],\n" +
"         \"place_id\" : \"ChIJDYPMBuwVqBQRS1K6RmEBZ3Y\",\n" +
"         \"rating\" : 4,\n" +
"         \"reference\" : \"CmRRAAAAKL6aik7tFa2HinCAvmYzPXNsigpVLhN5R7ULh_waWulx3PlPBrzYW54psESFbiAHk0Xa4GiuIsjhRcrNWToS-qbUm9AqPkFxKKYa43arxANGXyiJvpIZmHcrQhRe7CLMEhBi3PbCtdaHOcY_DUwQz_BJGhQpMxoQbbM9i0aTAv1hx3ZPO_fcTw\",\n" +
"         \"scope\" : \"GOOGLE\",\n" +
"         \"types\" : [ \"bar\", \"cafe\", \"food\", \"point_of_interest\", \"establishment\" ],\n" +
"         \"vicinity\" : \"Pavlou Kountouriotou 61, Perea\"\n" +
"      },\n" +
"      {\n" +
"         \"geometry\" : {\n" +
"            \"location\" : {\n" +
"               \"lat\" : 40.50954480000001,\n" +
"               \"lng\" : 22.92642090000001\n" +
"            }\n" +
"         },\n" +
"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/bar-71.png\",\n" +
"         \"id\" : \"896bb30a2ab6ebe19bc2c52b21f65b2c55703fff\",\n" +
"         \"name\" : \"Joy Coctail Bar, Perea\",\n" +
"         \"photos\" : [\n" +
"            {\n" +
"               \"height\" : 1920,\n" +
"               \"html_attributions\" : [\n" +
"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/104605312189584939935/photos\\\"\\u003eChristina Andromeda\\u003c/a\\u003e\"\n" +
"               ],\n" +
"               \"photo_reference\" : \"CoQBdwAAAPzD4qaLf_yiKQ0oKLMbnusHKyBxPhnsf-PPxbJ78weOC3OS_48cMk14rt4xtz2__QfhwXqradaDsjQxzhcGQd_RscANPUfdC08uZyhkMGuwK7C_fdf4nuvz6weCLn76IiGkExNkzf2VHw4VPHLbl43cYJFUSsQmEOV3kAZeGfafEhBGNOX9APHc0ass5OhoROZPGhRtN3hBAzVRxRPokNwH5UvoR5fSVQ\",\n" +
"               \"width\" : 2560\n" +
"            }\n" +
"         ],\n" +
"         \"place_id\" : \"ChIJt8JsXuwVqBQRc9p_CVlmsOs\",\n" +
"         \"reference\" : \"CmRSAAAAYicgm5fClcIplpWZ3hNhkyVHYGY6Ae40EURMW_VRfZlUGtWhVYFRk30P_hCClxQVk2oWdE9uPS4QT_7yq7M-tgy7hgB01NI2tBlJ5zdPSIWW5GS7oAP6o-0JgGXFfaeHEhCeyqD1rt4tgFBhHOUrdF_iGhSdlBpGpvRk5Jnv-XqUlURuIuW7Ug\",\n" +
"         \"scope\" : \"GOOGLE\",\n" +
"         \"types\" : [ \"bar\", \"point_of_interest\", \"establishment\" ],\n" +
"         \"vicinity\" : \"joy coctail bar perea\"\n" +
"      },\n" +
"      {\n" +
"         \"geometry\" : {\n" +
"            \"location\" : {\n" +
"               \"lat\" : 40.5091061,\n" +
"               \"lng\" : 22.9250497\n" +
"            },\n" +
"            \"viewport\" : {\n" +
"               \"northeast\" : {\n" +
"                  \"lat\" : 40.50926929999999,\n" +
"                  \"lng\" : 22.9250766\n" +
"               },\n" +
"               \"southwest\" : {\n" +
"                  \"lat\" : 40.5090517,\n" +
"                  \"lng\" : 22.924969\n" +
"               }\n" +
"            }\n" +
"         },\n" +
"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/bar-71.png\",\n" +
"         \"id\" : \"d10575630e658def49de9af284f0d32e9e474b25\",\n" +
"         \"name\" : \"Μεδουσα\",\n" +
"         \"photos\" : [\n" +
"            {\n" +
"               \"height\" : 1920,\n" +
"               \"html_attributions\" : [\n" +
"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/104605312189584939935/photos\\\"\\u003eChristina Andromeda\\u003c/a\\u003e\"\n" +
"               ],\n" +
"               \"photo_reference\" : \"CoQBdwAAAHIO8kjLetgVjI4K179Dfnvjdfc1fGJbfIN7fZnl6fD6Ay6lC4iMcIrRjd9s4p08jmn0Wm7_lGc2cX7oV3IN_-kOTGP-1JlurXcuLlAk_JPvyuZQHWnKCo1UFPJylGPtKNQVkdGlT11GargwzUPPJIUeutjSjBK7NPhrvjwOUkA4EhBXsCjumav5X0o-sZtKJpnMGhR0preYgoiCgtSOlyAsKMT3VV6mMA\",\n" +
"               \"width\" : 2560\n" +
"            }\n" +
"         ],\n" +
"         \"place_id\" : \"ChIJ8z6dDewVqBQRLaxcGxv_3g4\",\n" +
"         \"reference\" : \"CmRRAAAAx4VYgI280Jc8816IXtZ-Tx4EC0aYMMW_G3WRR786ur31uq-4pjXnpR2PI2Oxkgs8XqVwSNYoW2xYTOpAtkMibOnqXF08fQoYTeeiUmMdq3rkZFNpZQp07Pj9bzUM25eTEhAqkdgQKDq1VOg3mRQ291hbGhQpxR7cgb3y5nUJudSkdo4O8wgQIg\",\n" +
"         \"scope\" : \"GOOGLE\",\n" +
"         \"types\" : [ \"bar\", \"point_of_interest\", \"establishment\" ],\n" +
"         \"vicinity\" : \"Pavlou Kountouriotou 73, Perea\"\n" +
"      },\n" +
"      {\n" +
"         \"geometry\" : {\n" +
"            \"location\" : {\n" +
"               \"lat\" : 40.50157799999999,\n" +
"               \"lng\" : 22.9293817\n" +
"            },\n" +
"            \"viewport\" : {\n" +
"               \"northeast\" : {\n" +
"                  \"lat\" : 40.50165604999999,\n" +
"                  \"lng\" : 22.9293955\n" +
"               },\n" +
"               \"southwest\" : {\n" +
"                  \"lat\" : 40.50153345,\n" +
"                  \"lng\" : 22.92935749999999\n" +
"               }\n" +
"            }\n" +
"         },\n" +
"         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png\",\n" +
"         \"id\" : \"a6a33062247e80d24e6ff20dc469d9c32b40af56\",\n" +
"         \"name\" : \"Lyco Lounge\",\n" +
"         \"photos\" : [\n" +
"            {\n" +
"               \"height\" : 2560,\n" +
"               \"html_attributions\" : [\n" +
"                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/105355835927640564845/photos\\\"\\u003eΚΑΛΠΑΚΙΔΟΥ ΦΩΤΕΙΝΗ\\u003c/a\\u003e\"\n" +
"               ],\n" +
"               \"photo_reference\" : \"CoQBdwAAAMo9w0828VLGFj0cC9OGavNm06oZian5xuHpp78LP_lxmeqtpiBYEb_iV6ehy0KvPPJaK_QziI2Lpx0dbDKmCVYFB_P7_z7Nrn8FhLLS6RdZuTUdns4QVK8PwE0-zPIQznFb-eq8JHQ0UfYDjjwzzRJJbmw4wb79qsZSk3PBUy4hEhBaXfhNm-SxaZ9CJvb55BRiGhSp_j7IxLn83RZwOWIK945Jajpdfw\",\n" +
"               \"width\" : 1920\n" +
"            }\n" +
"         ],\n" +
"         \"place_id\" : \"ChIJdTCudMAVqBQR4x6ZeZhMnFg\",\n" +
"         \"reference\" : \"CmRRAAAA67yAN6JhvzVKW-46F5ySSNfZlN8_yoeh9oHfmUk2QU6SBxtQQwPLJjp3r20qnXUS9AEL9oUKemYblSB29QZ0DbSbCGFXBMdToxZhdEAuxrXqV0mGqv3dlyB7L2axA6rJEhDgQxmPYrdXL1aRr85IdONjGhTEklAO3zrXfXeg4v6qiBXKc1d38w\",\n" +
"         \"scope\" : \"GOOGLE\",\n" +
"         \"types\" : [\n" +
"            \"restaurant\",\n" +
"            \"cafe\",\n" +
"            \"bar\",\n" +
"            \"food\",\n" +
"            \"point_of_interest\",\n" +
"            \"establishment\"\n" +
"         ],\n" +
"         \"vicinity\" : \"Leoforos Thessalonikis - Michanionas 52, Perea\"\n" +
"      }\n" +
"   ],\n" +
"   \"status\" : \"OK\"\n" +
"}";
    
    
    private static final String mockJsonLocation = "{\n" +
"   \"results\" : [\n" +
"      {\n" +
"         \"address_components\" : [\n" +
"            {\n" +
"               \"long_name\" : \"8\",\n" +
"               \"short_name\" : \"8\",\n" +
"               \"types\" : [ \"street_number\" ]\n" +
"            },\n" +
"            {\n" +
"               \"long_name\" : \"Alexandrou Papanastasiou\",\n" +
"               \"short_name\" : \"Al. Papanastasiou\",\n" +
"               \"types\" : [ \"route\" ]\n" +
"            },\n" +
"            {\n" +
"               \"long_name\" : \"Kalamaria\",\n" +
"               \"short_name\" : \"Kalamaria\",\n" +
"               \"types\" : [ \"locality\", \"political\" ]\n" +
"            },\n" +
"            {\n" +
"               \"long_name\" : \"Kalamaria\",\n" +
"               \"short_name\" : \"Kalamaria\",\n" +
"               \"types\" : [ \"administrative_area_level_5\", \"political\" ]\n" +
"            },\n" +
"            {\n" +
"               \"long_name\" : \"Kalamaria\",\n" +
"               \"short_name\" : \"Kalamaria\",\n" +
"               \"types\" : [ \"administrative_area_level_4\", \"political\" ]\n" +
"            },\n" +
"            {\n" +
"               \"long_name\" : \"Thessaloniki\",\n" +
"               \"short_name\" : \"Thessaloniki\",\n" +
"               \"types\" : [ \"administrative_area_level_3\", \"political\" ]\n" +
"            },\n" +
"            {\n" +
"               \"long_name\" : \"Greece\",\n" +
"               \"short_name\" : \"GR\",\n" +
"               \"types\" : [ \"country\", \"political\" ]\n" +
"            },\n" +
"            {\n" +
"               \"long_name\" : \"551 32\",\n" +
"               \"short_name\" : \"551 32\",\n" +
"               \"types\" : [ \"postal_code\" ]\n" +
"            }\n" +
"         ],\n" +
"         \"formatted_address\" : \"Al. Papanastasiou 8, Kalamaria 551 32, Greece\",\n" +
"         \"geometry\" : {\n" +
"            \"bounds\" : {\n" +
"               \"northeast\" : {\n" +
"                  \"lat\" : 40.5737739,\n" +
"                  \"lng\" : 22.9539518\n" +
"               },\n" +
"               \"southwest\" : {\n" +
"                  \"lat\" : 40.5737642,\n" +
"                  \"lng\" : 22.9539379\n" +
"               }\n" +
"            },\n" +
"            \"location\" : {\n" +
"               \"lat\" : 40.5737642,\n" +
"               \"lng\" : 22.9539518\n" +
"            },\n" +
"            \"location_type\" : \"RANGE_INTERPOLATED\",\n" +
"            \"viewport\" : {\n" +
"               \"northeast\" : {\n" +
"                  \"lat\" : 40.5751180302915,\n" +
"                  \"lng\" : 22.9552938302915\n" +
"               },\n" +
"               \"southwest\" : {\n" +
"                  \"lat\" : 40.5724200697085,\n" +
"                  \"lng\" : 22.9525958697085\n" +
"               }\n" +
"            }\n" +
"         },\n" +
"         \"partial_match\" : true,\n" +
"         \"place_id\" : \"Ei1BbC4gUGFwYW5hc3Rhc2lvdSA4LCBLYWxhbWFyaWEgNTUxIDMyLCBIZWxsYXM\",\n" +
"         \"types\" : [ \"street_address\" ]\n" +
"      }\n" +
"   ],\n" +
"   \"status\" : \"OK\"\n" +
"}";
    
}
